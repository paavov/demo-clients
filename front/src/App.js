import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';

import DemoPage from './containers/DemoPage/DemoPage';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <DemoPage />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
