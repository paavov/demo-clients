import React, { Component } from 'react';
import { Route, NavLink, Switch, Redirect } from 'react-router-dom';
import axios from 'axios';

import classes from './DemoPage.css';
import Clients from './Clients/Clients';
import SavedClients from './SavedClients/SavedClients';
import ClientView from './ClientView/ClientView';

class DemoPage extends Component {
    state = {
        clients: null
    }

    componentDidMount () {
        axios.get('http://www.filltext.com/?rows=100&pretty=true&id=%7bindex%7d&name=%7bbusiness%7d&address=%7baddressObject%7d')
            .then(res => {
                const clients = res.data.slice(0, 100);
                const updatedClients = clients.map(client => {
                    return {
                        ...client,
                        saved: false
                    }
                });
                this.setState({clients: updatedClients});
                console.log(updatedClients);
            })
            .catch(err => {
                console.log(err);
            });
    }

    render () {
        return (
            <div className={classes.DemoPage}>
                <header>
                    <nav>
                        <ul>
                        <li><NavLink
                            to="/clients"
                            exact
                            activeStyle={{
                                color: '#fa923f'
                            }}>Asiakkaat</NavLink></li>
                        <li><NavLink
                            to="/clients/saved"
                            exact
                            activeStyle={{
                                color: '#fa923f'
                            }}>Tallennetut</NavLink></li>
                        <li><NavLink
                            className={classes.Disabled}
                            to="/clients/client/"
                            activeStyle={{
                                color: '#fa923f'
                            }}>Asiakas</NavLink></li>
                        </ul>
                    </nav>
                </header>
                <Switch>
                <Route path="/clients" exact component={(props) => <Clients {...props} clients={this.state.clients} />}  />
                <Route path="/clients/saved" component={SavedClients} />
                <Route path="/clients/client/:id" component={ClientView} />
                <Redirect from="/" to="/clients" />
                <Route render={() => <h1>Not found</h1>} />
                </Switch>
            </div>
        );
    }
}

export default DemoPage;