import React, { Component } from 'react';
import axios from 'axios';
import queryString from 'query-string';

import Client from '../../../components/Client/Client';
import Spinner from '../../../components/UI/Spinner/Spinner';
import classes from '../Clients/Clients.css'; 

class SavedClients extends Component {
    state = {
        savedClients: [],
        loading: false,
        error: false,
        wasClicked: false
    }

    componentDidMount () {
        console.log(this.props);
        this.setState({loading: true});
        axios.get('https://localhost:5001/api/clients')
            .then(res => {
                const clients = res.data.slice(0, 100);
                const updatedClients = clients.map(client => {
                    return {
                        ...client
                    }
                });
                this.setState({savedClients: updatedClients, loading: false});
                console.log(updatedClients);
            })
            .catch(err => {
                console.log(err);
                this.setState({error: true});
            });
    }

    componentWillUpdate () {
        if ( this.state.wasClicked ) {
        axios.get('https://localhost:5001/api/clients')
            .then(res => {
                const clients = res.data.slice(0, 100);
                const updatedClients = clients.map(client => {
                    return {
                        ...client
                    }
                });
                this.setState({savedClients: updatedClients, loading: false, wasClicked: false});
                console.log('From willUpdate' + updatedClients);
            })
            .catch(err => {
                console.log(err);
                this.setState({error: true});
            });
        } else return;
    }

    deleteClientHandler = ( client ) => {
        console.log(client.id);
        const options = {
            method: 'DELETE',
            headers: { 'content-type': 'application/json' },
            url: 'https://localhost:5001/api/clients/' + client.id
          };
        axios(options)
        .then(res => {
            console.log(res);
            const updatedClients = this.state.savedClients.splice(client, 1);
            this.setState({savedClients: updatedClients, loading: true});
        })
        .catch(err => {
            console.log(err);
            this.setState({error: true});
        });
    }

    showClientHandler = ( client ) => {
        const query = {
            name: client.name, 
            address: client.addressStreetAddress,
            city: client.addressCity,
            state: client.addressState,
            zip: client.addressZip}
        
        const searchString = queryString.stringify( query );

        this.props.history.push({
            pathname: '/clients/client/' + client.id,
            search: searchString
        });
    }

    changeOrderHandler = ( asc ) => {
        const updatedClients = this.state.savedClients;
        if ( asc ) {
            updatedClients.sort((a, b) => a.name.localeCompare(b.name));
        } else {
            updatedClients.sort((a,b) => a.name.localeCompare(b.name)).reverse();
        }
        this.setState({ savedClients: updatedClients });
    }
    
    render () {
        let savedClients = null;

        if (this.state.savedClients) {
            savedClients = this.state.savedClients.map(client => {
                return (
                    <Client 
                        key={client.id}
                        name={client.name}
                        btn={"POISTA"}
                        btnClicked={() => {this.deleteClientHandler( client ); this.setState({wasClicked: true});}}
                        clicked={() => this.showClientHandler( client )} />
                );
            });
        }
        if (this.state.loading || this.state.error) {
            savedClients = <Spinner />;
        }
        return (
            <div>
                <h1 style={{textAlign: "center"}}>Tallennetut asiakkaat</h1>
                <div className={classes.SortButtons}>
                    <button onClick={() => this.changeOrderHandler( true )}>A -> Z</button>
                    <button onClick={() => this.changeOrderHandler( false )}>Z -> A</button>
                </div>
                <section className={classes.Clients}>
                    { savedClients }
                </section>
            </div>
        );
    }
}

export default SavedClients;