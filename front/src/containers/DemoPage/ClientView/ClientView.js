import React, { Component } from 'react';
import queryString from 'query-string';

import classes from './ClientView.css';

class ClientView extends Component {
    render () {
        return (
            <div className={classes.ClientView}>
                <h1>Asiakkaan tiedot</h1>
                <section>
                    <p><strong>Nimi: </strong>{queryString.parse(this.props.location.search).name}</p>
                    <p><strong>Osoite: </strong>{queryString.parse(this.props.location.search).address}</p>
                    <p><strong>Kaupunki: </strong>{queryString.parse(this.props.location.search).city}</p>
                    <p><strong>Osavaltio: </strong>{queryString.parse(this.props.location.search).state} </p>
                    <p><strong>Postinumero: </strong>{queryString.parse(this.props.location.search).zip}</p>
                </section>
            </div>
        );
    }
}

export default ClientView;