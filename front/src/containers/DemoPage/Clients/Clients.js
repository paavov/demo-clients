import React, { Component } from 'react';
import axios from 'axios';
import queryString from 'query-string';

import Client from '../../../components/Client/Client';
import classes from './Clients.css';

class Clients extends Component {
    state = {
        loading: true,
        fetchedClients: null
    }

    componentDidMount () {
        console.log(this.props);
        this.setState({fetchedClients: this.props.clients});
    }

    saveClientHandler = ( client ) => {
        const index = this.state.fetchedClients.indexOf( client );
        const newState = this.state.fetchedClients;
        const data = {
            id: client.id,
            name: client.name,
            addressStreetAddress: client.address.streetAddress,
            addressCity: client.address.city,
            addressState: client.address.state,
            addressZip: parseInt(client.address.zip, 10)
        };
        const options = {
            method: 'POST',
            headers: { 'content-type': 'application/json' },
            data: data,
            url: 'https://localhost:5001/api/clients'
          };
        console.log(data);
        axios(options)
        .then(res => {
            console.log(res);
            newState[index].saved = true;
            this.setState({ fetchedClients: newState });
        })
        .catch(err => {
            console.log(err);
        });
    }

    showClientHandler = ( client ) => {
        const query = {
            name: client.name, 
            address: client.address.streetAddress,
            city: client.address.city,
            state: client.address.state,
            zip: client.address.zip
        }
        
        const searchString = queryString.stringify( query );

        this.props.history.push({
            pathname: '/clients/client/' + client.id,
            search: searchString
        });
    }

    changeOrderHandler = ( asc ) => {
        const updatedClients = this.state.fetchedClients;
        if ( asc ) {
            updatedClients.sort((a, b) => a.name.localeCompare(b.name));
        } else {
            updatedClients.sort((a,b) => a.name.localeCompare(b.name)).reverse();
        }
        this.setState({ fetchedClients: updatedClients });
    }

    render () {
        let clients = null;
        if (this.state.fetchedClients) {
                clients = this.state.fetchedClients.map(client => {
                    if (!client.saved) {
                        return (
                            <Client 
                                key={client.id}
                                name={client.name}
                                btn={"TALLENNA"}
                                btnClicked={() =>this.saveClientHandler( client )}
                                clicked={() => this.showClientHandler( client )} />
                        );
                     }
            });
        }

        return (
            <div>
                <h1 style={{textAlign: "center"}}>Asiakkaat</h1>
                <div className={classes.SortButtons}>
                    <button onClick={() => this.changeOrderHandler( true )}>A -> Z</button>
                    <button onClick={() => this.changeOrderHandler( false )}>Z -> A</button>
                </div>
                <section className={classes.Clients}>
                    { clients }
                </section>
            </div>
        );
    }
}

export default Clients;