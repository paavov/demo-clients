import React from 'react';

import classes from './Client.css';

const client = ( props ) => (
            <div className={classes.Client}>
                <h4 onClick={props.clicked}>{props.name}</h4>
                <button onClick={props.btnClicked}>{props.btn}</button>
            </div>
);
export default client;