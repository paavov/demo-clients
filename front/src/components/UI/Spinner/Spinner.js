import React from 'react';

import classes from './Spinner.css';

const spinner = () => (
    <div>
    <div className={classes.Loader}>Loading...</div>
    <p>Haetaan tietoja, pieni hetki...</p>
    </div> 
);

export default spinner;
