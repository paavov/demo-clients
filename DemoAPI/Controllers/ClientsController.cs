﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using DemoAPI.Models.DB;
using System.Threading.Tasks;

namespace DemoAPI.Controllers
{
    [Route("api/clients")]
    [ApiController]
    public class ClientsController : ControllerBase
    {
        private readonly demodbContext _context;

        public ClientsController(demodbContext context)
        {
            _context = context;
        }

        //GET: Clients/GetAll
        [HttpGet]
        public ActionResult<List<Client>> GetAll()
        {
            return _context.Client.ToList();
        }

        //GET: Clients/GetById/{id}
        [HttpGet("{id}", Name = "GetClients")]
        public ActionResult<Client> GetById(int id)
        {
            var client = _context.Client.Find(id);
            if (client == null)
            {
                return NotFound();
            }
            return client;
        }

        // POST: Clients/Create
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Client client)
        {
            if (ModelState.IsValid)
            {
                _context.Add(client);
                await _context.SaveChangesAsync();
                return CreatedAtRoute("GetClients", new { id = client.Id }, client);
            }

            return NotFound(new { message = "Could not save client." });
        }

        //DELETE: Clients/Delete/{id}
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var client = _context.Client.Find(id);
            if (client == null)
            {
                return NotFound();
            }

            _context.Client.Remove(client);
            _context.SaveChanges();
            return NoContent();
        }

    }
}