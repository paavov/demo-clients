﻿using Microsoft.EntityFrameworkCore;

namespace DemoAPI.Models.DB
{
    public partial class demodbContext : DbContext
    {
        public demodbContext()
        {
        }

        public demodbContext(DbContextOptions<demodbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Client> Client { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client>(entity =>
            {
                entity.ToTable("client");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.AddressCity)
                    .IsRequired()
                    .HasColumnName("address_city")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.AddressState)
                    .IsRequired()
                    .HasColumnName("address_state")
                    .HasColumnType("varchar(2)");

                entity.Property(e => e.AddressStreetAddress)
                    .IsRequired()
                    .HasColumnName("address_streetAddress")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.AddressZip)
                    .HasColumnName("address_zip")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(30)");
            });
        }
    }
}
