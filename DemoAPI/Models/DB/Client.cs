﻿using System;
using System.Collections.Generic;

namespace DemoAPI.Models.DB
{
    public partial class Client
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string AddressStreetAddress { get; set; }
        public string AddressCity { get; set; }
        public string AddressState { get; set; }
        public int AddressZip { get; set; }
    }
}
